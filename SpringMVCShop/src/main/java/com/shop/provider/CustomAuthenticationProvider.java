package com.shop.provider;

import com.shop.DAO.UserDAO;
import com.shop.domain.User;
import com.shop.manager.UserManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private UserManager userManager;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String name = authentication.getName();
        String password = authentication.getCredentials().toString();

        User user = new User(name, password);
        User foundUser = userDAO.auth(user);

        logger.info("Name = " + name + " ,Password = " + password);


        if (("admin".equals(name) && "admin".equals(password))) {
            logger.info("Succesful authentication!");
            List<SimpleGrantedAuthority> list = new ArrayList<>();
            list.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
            return new UsernamePasswordAuthenticationToken(name, password, list);
        }

        if (foundUser != null) {
            if (foundUser.getPassword().equals(password)) {
                userManager.setUser(foundUser);
                logger.info("Succesful authentication!");
                List<SimpleGrantedAuthority> list = new ArrayList<>();
                list.add(new SimpleGrantedAuthority("ROLE_USER"));
                return new UsernamePasswordAuthenticationToken(name, password, list);
            }
        }

        logger.info("Login fail!");

        return null;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

    public void findU() {
        if (userManager.getUser() == null) {
            userManager.setUser(new User("JohnCoolAF", "123"));
        }
    }
}
