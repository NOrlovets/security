package com.shop.controller;

import com.shop.DAO.UserDAO;
import com.shop.manager.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

//import com.shop.service.UserService;


@Controller
public class AuthController {

    @Autowired
    private UserManager userManager;

    @Autowired
    private UserDAO userDAO;

    @GetMapping("/auth")
    public String authGet(@RequestParam(value = "mess", required = false) String s, Model model) {
        if (s != null)
            model.addAttribute("mess", "You are now logged out");
        return "auth";
    }


//    @RequestMapping("/auth")
//    public String authGetPar(@RequestParam(value = "mess") String mess, Model model) {
//        System.out.println("123");
//        model.addAttribute("mess", "You are now logged out");
//        return "auth";
//    }

//    @PostMapping("/auth")
//    public String authPost(@Valid User user, BindingResult bindingResult) {
//        if (bindingResult.hasErrors()) {
//            return "auth";
//        }
//        User foundUser = userDAO.auth(user);
//        if (foundUser != null) {
//            if (foundUser.getPassword().equals(user.getPassword())) {
//                userManager.setUser(foundUser);
//            }
//        }
//        return "redirect:/main";
//    }

}
