package com.shop.controller;

import com.shop.DAO.ProductDAO;
import com.shop.domain.User;
import com.shop.manager.UserManager;
import com.shop.provider.CustomAuthenticationProvider;
import com.shop.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

    @Autowired
    private ProductService productService;

    @Autowired
    CustomAuthenticationProvider customAuthenticationProvider;

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private UserManager userManager;

    @GetMapping("/main")
    public String greeting(Model model) {
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        User user = (User) authentication.getPrincipal();
        customAuthenticationProvider.findU();
        model.addAttribute("name", userManager.getUser().getLogin());
        //model.addAttribute("list", productDAO.getProducts());
        model.addAttribute("list", productService.findAll());
        return "main";
    }

//    @GetMapping("/logoutt")
//    public String logout(Model model) {
//        model.addAttribute("mess", "You are now logged out");
//        return "auth";
//        //session.invalidate();
//        //return "redirect:/auth";
//    }

}
