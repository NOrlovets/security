package com.shop.config;

import com.shop.provider.CustomAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CustomAuthenticationProvider authProvider;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.authorizeRequests()
                .antMatchers("/css/**", "/js/**", "/img/**", "/WEB-INF/**").permitAll()
                .antMatchers("/reg", "/auth").permitAll()
                .antMatchers("/main").hasRole("USER")
                .antMatchers("/admin").hasRole("ADMIN")
                .and().formLogin().loginPage("/auth")
                .usernameParameter("j_username")
                .passwordParameter("j_password")
                .and().rememberMe()
                .key("uniqueAndSecret")
                .rememberMeParameter("remember-me")
                .rememberMeCookieName("javasampleapproach-remember-me")
                .tokenValiditySeconds(24 * 60 * 60)
                .and().logout().logoutUrl("/logout").deleteCookies("JSESSIONID")
                .logoutSuccessUrl("/auth?mess=succ")
                //.logoutSuccessUrl("/logoutt")
                .permitAll();
        http.exceptionHandling().accessDeniedPage("/auth");
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().withUser("JohnCoolAF").password("123").roles("USER");
    }
}
