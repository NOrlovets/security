package com.shop.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "cart")
public class Cart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_cart")
    private int id;
    @Column(name = "sum")
    private int sum;
    @Column(name = "discount")
    private int discount;
    @Column(name = "id_user")
    private int userId;


    public Cart() {
    }

    public Cart(int sum, int discount, int userId) {
        this.sum = sum;
        this.discount = discount;
        this.userId = userId;
    }

    public Cart(int id, int sum, int discount, int userId) {
        this.id = id;
        this.sum = sum;
        this.discount = discount;
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    //    public List<Product> getProducts() {
//        return products;
//    }
//
//    public void setProducts(Product product) {
//        this.products.add(product);
//    }
//
//    @ManyToMany(cascade=CascadeType.ALL)
//    @JoinTable(
//            name="cart_has_product",
//            joinColumns = @JoinColumn(name = "id_cart"),
//            inverseJoinColumns = @JoinColumn(name = "id_product")
//    )
//    List<Product> products = new ArrayList<>();
}
