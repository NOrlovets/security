package com.shop.domain;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_product")
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "price")
    private int price;
    @Column(name = "amount")
    private int amount;
    @Column(name = "id_brand")
    private int idBrand;
    @Column(name = "id_category")
    private int idCategory;

//    @ManyToMany(mappedBy = "products")
//    private List<Cart> carts = new ArrayList<>();

    public Product() {
    }

    public Product(String name, int price, int amount, int idBrand, int idCategory) {
        this.name = name;
        this.price = price;
        this.amount = amount;
        this.idBrand = idBrand;
        this.idCategory = idCategory;
    }

//    public List<Cart> getCarts() {
//        return carts;
//    }
//
//    public void setCarts(Cart cart) {
//        this.carts.add(cart);
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getIdBrand() {
        return idBrand;
    }

    public void setIdBrand(int idBrand) {
        this.idBrand = idBrand;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(int idCategory) {
        this.idCategory = idCategory;
    }

}
