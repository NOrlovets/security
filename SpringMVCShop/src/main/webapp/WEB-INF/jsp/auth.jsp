<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="css/themes.css">
    <title>Auth</title>
</head>
<body>

<div class="login-page">
    <div class="form">
        <h1 class="upmessage">Authorization</h1>
        <p style="text-align: center">${mess}</p>
        <form action="auth" method="POST">
            <input type="text" name="j_username" placeholder="login"/>
            <input type="password" name="j_password" placeholder="password"/>
<div>Remember Me: <input type="checkbox" name="remember-me" /> </div>
            <button>login</button>
        </form>

        <p class="message"><a href="reg">Registration</a></p>
    </div>
</div>

</body>
</html>
