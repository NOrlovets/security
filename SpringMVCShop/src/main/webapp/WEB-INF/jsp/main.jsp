<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
        <%@ page contentType="text/html;charset=UTF-8" language="java" %>
            <html>

            <head>
                <link rel="stylesheet" href="css/themes.css">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
                <script src="js/script.js"></script>


                <title>University of Epam</title>
            </head>

            <body>
                <nav class="navbar navbar-inverse navbar-fixed-top">
                    <div class="container-fluid">
                        <ul class="nav navbar-nav">
                            <li><a href="#"><span class="glyphicon glyphicon-user"> ${name}</span></a></li>
                            <li><a href="/logout"><span class="glyphicon glyphicon-log-out"> Exit</span></a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <button type="button" class="btn btn-info reg"> Search..</button>
                            </li>
                            <p class="navbar-text" id="cart">0</p>
                            <li>
                                <button type="button" class="btn btn-primary reg"> Shop</button>
                            </li>
                        </ul>
                    </div>
                </nav>




                <div id="blur">
                    <dl class="authors">
                        <dt>Food</dt>
                        <c:forEach items="${list}" var="item">
                            <c:if test="${item.idCategory == 1}">
                                <div class="box button">
                                    <img src="https://farm8.staticflickr.com/7326/11287113923_57d37ed9d3_q.jpg" />
                                   <p class="name">  <c:out value="${item.name}" /></p>
                                    <p class="price" style="display: none"><c:out value="${item.price}" /></p>
                                    <p class="id" style="display: none"><c:out value="${item.id}" /></p>
                                </div>
                            </c:if>
                        </c:forEach>

                        <dt>Clothes</dt>
                        <c:forEach items="${list}" var="item">
                            <c:if test="${item.idCategory == 2}">
                                    <div class="box button">
                                        <img src="https://farm8.staticflickr.com/7326/11287113923_57d37ed9d3_q.jpg" />

                                          <p class="name">  <c:out value="${item.name}" /></p>
                                            <p class="price" style="display: none"><c:out value="${item.price}" /></p>
                                            <p class="id" style="display: none"><c:out value="${item.id}" /></p>

                                    </div>
                                </c:if>
                            </c:forEach>
                    </dl>

                </div>
                <div class="product description">
                    <span>✖</span>

                    <img src="https://farm8.staticflickr.com/7326/11287113923_57d37ed9d3_q.jpg" id="img" />
                    <div class="prodname"></div>
                    <div class="prodid" style="display: none"></div>
                    <div class="prodprice"></div>
                    <button type="button" name="button" id="butcen">Buy</button>

                </div>

                <div class="search">
                    <span>✖</span>

                    <h1>Search</h1>

                    <form>
                        <input type="text" id="searchinp">
                        <input type="button" id="sbutton" value="Find">
                        <%--<form:button id="sbutton">Find</form:button>--%>
                    </form>
                    <div id="result">
                        <ul id="sul-id"></ul>
                    </div>

                </div>

                <div class="shop">
                    <span>✖</span>

                    <div id="shopcart">
                        <ul id="ul-id"></ul>
                    </div>

                </div>

                <div class="footer">This footer will always be positioned at the bottom of the page</div>


            </body>

            </html>